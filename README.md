The original version of dwm can be found here: [Suckless website](http://dwm.suckless.org/)

# Features/Patches

Each patch referred in this section is present in the patches folder.

 + Autostart - Run applications at startup using the file in `$HOME/.dwm/autostart.sh`
 + Rotatestack - Move tiled clients in a circular motion (counter clockwise)
 + Attachaside - Spawned client are positioned on the stack stacking area instead of the master area
 + Spawn Command - Every new terminal spawned will inherit the current working directory from the previously focused client
   + This patch was updated by me to support dwm 6.1 and includes a fix for symlink directories
 + Pertag - saves various variables for each tag, such as layout mode, mfactor and nmaster.
 + Incnmaster Bounded - Custom patch that fixes two problems I have with vanilla dwm
   + Decrease nmaster instantly - When increasing the number of master clients (nmaster) in vanilla dwm, this variable can surpass the number of present clients on the tag. As a result, to remove a client from the master area (first column) to the stacking area (second column), the user is required to hold the Mod+D key until the nmaster variable is in a usable range. This patch makes it so that whenever the user decreases the nmaster variable, a client is always moved from the master area to the stacking area. In other words the nmaster variable is bounded from 0 to the current amount of clients in the tag.
 + Truefullscreen - Custom patch for i3wm like fullscreen behavior, which can be used to replace the monocle layout mode.
 + Bettergaps - a custom version of the default gaps patch for outer gaps. Additionally the user can specify if the WM starts in gaps mode.

# Installation

Run the following command in the dwm folder: `sudo make install`
