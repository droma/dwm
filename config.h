/* See LICENSE file for copyright and license details. */
#include <X11/XF86keysym.h>

/* appearance */
static const unsigned int borderpx  = 3;        /* border pixel of windows */
static const unsigned int gappx     = 10;       /* outer and inner */
static const unsigned int snap      = 32;       /* snap pixel */
static const int start_with_gaps    = 0;        /* 1 to start with gaps */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = {"InputNerdFontCompleteMono Nerd Font:size=11"};
static const char dmenufont[]       = "InputNerdFontCompleteMono Nerd Font:size=11";
static const char col_gray1[]       = "#101010";
static const char col_gray2[]       = "#444444";
static const char col_gray3[]       = "#bbbbbb";
static const char col_gray4[]       = "#eeeeee";
static const char col_cyan[]        = "#005577";
static const char *colors[][3] = {
	/*               fg         bg         border    */
	[SchemeNorm] = { col_gray3, col_gray1, col_gray2 },
	[SchemeSel]  = { col_gray4, col_cyan,  col_cyan  },
};
										   /* W     H */
static const int default_floating_size[] = { 860, 600 };

/* tagging */
static const char *tags[] = {"web",  "mail", "mus",  "vid",  "work",
                             "view", "game", "chat", "file", "term"};

static const Rule rules[] = {
        /* xprop(1):
         *	WM_CLASS(STRING) = instance, class
         *	WM_NAME(STRING) = title
         */
        /* class         instance   title          tags mask   isfloating   monitor */
        { "pop-up",      NULL,      NULL,          0,          1,           -1 },
        { "Nitrogen",    NULL,      NULL,          0,          1,           -1 },
        { "firefox",     NULL,      NULL,          1,          0,           -1 },
        { "qutebrowser", NULL,      NULL,          1,          0,           -1 },
        { "thunderbird", NULL,      NULL,          1 << 1,     0,           -1 },
        { "Steam",       NULL,      NULL,          1 << 6,     0,           -1 },
        { "Skype",       NULL,      NULL,          1 << 7,     0,           -1 },
        { NULL,          NULL,      "WhatsApp",    1 << 7,     0,           -1 },
};

/* layout(s) */
static const float mfact     = 0.5; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;   /* number of clients in master area */
static const int resizehints = 0;   /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
        /* symbol     arrange function */
		{ "[]=",      tile },    /* first entry is default */
		{ "[M]",      monocle },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY, TAG) \
	{MODKEY,                       KEY,      view,          {.ui = 1 << TAG}}, \
	{MODKEY|ControlMask,           KEY,      toggleview,    {.ui = 1 << TAG}}, \
	{MODKEY|ShiftMask,             KEY,      tag,           {.ui = 1 << TAG}}, \
	{MODKEY|ControlMask|ShiftMask, KEY,      toggletag,     {.ui = 1 << TAG}},

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char *[]) { "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
const char *dmenuflags[] = { "-m",      dmenumon, "-fn", dmenufont, "-nb",
                          col_gray1, "-nf",     col_gray3, "-sb",     col_cyan,
                          "-sf",     col_gray4, NULL };
static const char *dmenucmd[] = { "dmenu_run", "-m",  dmenumon, "-fn", dmenufont,
	"-nb", col_gray1, "-nf", col_gray3,   "-sb", col_cyan, "-sf", col_gray4, NULL };
static const char *passmenucmd[] = { "passmenu", "-m",  dmenumon, "-fn", dmenufont,
	"-nb", col_gray1, "-nf", col_gray3,  "-sb", col_cyan, "-sf", col_gray4, NULL };
static const char *quitpromptcmd[] = { "/home/droma/.dwm/exit_menu.sh", "-m", dmenumon,
	"-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_cyan, "-sf",
	col_gray4, NULL };
static const char *netwmngcmd[] = { "networkmanager_dmenu", "-m", dmenumon, "-fn",
	dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_cyan, "-sf", col_gray4,
	NULL };
//static const char *termcmd[] = { "st", NULL };
// static const char *termcmd[] = { "urxvt", NULL };
static const char *termcmd[] = { "alacritty", NULL };
static const char *browsercmd[] = { "firefox", NULL };
static const char *mailcmd[] = { "thunderbird", NULL };
static const char *lowervolcmd[] = { "amixer", "-q", "set", "Master", "5%-", "unmute",
	NULL };
static const char *raisevolcmd[] = { "amixer", "-q", "set", "Master", "5%+", "unmute",
	NULL };
static const char *mutecmd[] = { "pactl", "set-sink-mute", "0", "toggle", NULL };
static const char *togglemuscmd[] = { "bash", "/home/droma/.i3/scripts/toggle_music.sh",
	NULL };
static const char *nextmuscmd[] = { "bash", "/home/droma/.i3/scripts/next_song.sh",
	NULL };
static const char *prevmuscmd[] = { "bash", "/home/droma/.i3/scripts/prev_song.sh",
	NULL };
static const char *brightupcmd[] = { "light", "-A", "10", NULL };
static const char *brightdowncmd[] = { "light", "-U", "10", NULL };
static const char *lockcmd[] = { "betterlockscreen", "-l", NULL };

static Key keys[] = {
	/* modifier         key                       function           argument */
	{ MODKEY,           XK_d,                     spawn,             {.v = dmenucmd} },
	{ MODKEY,           XK_w,                     spawn,             {.v = browsercmd} },
	{ MODKEY,           XK_m,                     spawn,             {.v = mailcmd} },
	{ 0,                XF86XK_AudioLowerVolume,  spawn,             {.v = lowervolcmd} },
	{ 0,                XF86XK_AudioRaiseVolume,  spawn,             {.v = raisevolcmd} },
	{ 0,                XF86XK_AudioMute,         spawn,             {.v = mutecmd} },
	{ 0,                XF86XK_MonBrightnessUp,   spawn,             {.v = brightupcmd} },
	{ 0,                XF86XK_MonBrightnessDown, spawn,             {.v = brightdowncmd} },
	{ 0,                XF86XK_AudioPlay,         spawn,             {.v = togglemuscmd} },
	{ 0,                XF86XK_AudioNext,         spawn,             {.v = nextmuscmd} },
	{ 0,                XF86XK_AudioPrev,         spawn,             {.v = prevmuscmd} },
	{ MODKEY,           XK_Return,                spawn,             {.v = termcmd} },
	{ MODKEY,           XK_Delete,                spawn,             {.v = lockcmd} },
	{ MODKEY,           XK_p,                     spawn,             {.v = passmenucmd} },
	{ MODKEY,           XK_n,                     spawn,             {.v = netwmngcmd} },
	{ MODKEY|ShiftMask, XK_e,                     spawn,             {.v = quitpromptcmd} },
	{ MODKEY,           XK_b,                     togglebar,         {0} },
	{ MODKEY,           XK_g,                     togglegaps,        {0} },
	{ MODKEY,           XK_j,                     focusstack,        {.i = +1} },
	{ MODKEY,           XK_k,                     focusstack,        {.i = -1} },
	{ MODKEY|ShiftMask, XK_j,                     rotatestack,       {.i = +1} },
	{ MODKEY|ShiftMask, XK_k,                     rotatestack,       {.i = -1} },
	{ MODKEY,           XK_i,                     incnmasterbounded, {.i = +1} },
	{ MODKEY,           XK_u,                     incnmasterbounded, {.i = -1} },
	{ MODKEY,           XK_h,                     setmfact,          {.f = -0.05} },
	{ MODKEY,           XK_l,                     setmfact,          {.f = +0.05} },
	{ MODKEY|ControlMask|ShiftMask, XK_h,         resizefloatinghoriz,{.i = -25} },
	{ MODKEY|ControlMask|ShiftMask, XK_l,         resizefloatinghoriz,{.i = +25} },
	{ MODKEY|ControlMask|ShiftMask, XK_j,         resizefloatingvert,{.i = -25} },
	{ MODKEY|ControlMask|ShiftMask, XK_k,         resizefloatingvert,{.i = +25} },
	{ MODKEY|ControlMask,XK_h,                    movefloatinghoriz, {.i = -25} },
	{ MODKEY|ControlMask,XK_l,                    movefloatinghoriz, {.i = +25} },
	{ MODKEY|ControlMask,XK_k,                    movefloatingvert,  {.i = -25} },
	{ MODKEY|ControlMask,XK_j,                    movefloatingvert,  {.i = +25} },
	{ MODKEY,           XK_Tab,                   view,              {0} },
	{ MODKEY,           XK_q,                     killclient,        {0} },
	//{ MODKEY,           XK_f,                     setlayout,         {0} },
	{ MODKEY,           XK_f,                     togglefullscreen,  {0} },
	{ MODKEY|ShiftMask, XK_space,                 togglefloating,    {0} },
	TAGKEYS(            XK_1,                                        0)
	TAGKEYS(            XK_2,                                        1)
	TAGKEYS(            XK_3,                                        2)
	TAGKEYS(            XK_4,                                        3)
	TAGKEYS(            XK_5,                                        4)
	TAGKEYS(            XK_6,                                        5)
	TAGKEYS(            XK_7,                                        6)
	TAGKEYS(            XK_8,                                        7)
	TAGKEYS(            XK_9,                                        8)
	TAGKEYS(            XK_0,                                        9)
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
};
